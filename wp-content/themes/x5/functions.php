<?php
/**
 * X5: Theme specific functionalities
 *
 * Do not close any of the php files included with ?> closing tag!
 *
 * @package WordPress
 * @subpackage X5
 */

/*
 * Load X5 features
 */
function x5_load_features() {

	$features = scandir( dirname( __FILE__ ) . '/features/' );

	foreach ( $features as $feature ) {

		if ( current_theme_supports( $feature ) ) {
			require_once dirname( __FILE__ ) . '/features/' . $feature . '/' . $feature . '.php';
		}
	}
}

add_action( 'init', 'x5_load_features' );

/*
 * Add basic functionality required by WordPress.org
 */
if ( function_exists( add_theme_support( 'seo-title' ) ) ) {
	add_theme_support( 'seo-title' );
}

if ( function_exists( add_theme_support( 'threaded-comments' ) ) ) {
	add_theme_support( 'threaded-comments' );
}

if ( function_exists( add_theme_support( 'comments' ) ) ) {
	add_theme_support( 'comments' );
}

if ( function_exists( add_theme_support( 'automatic-feed-links' ) ) ) {
	add_theme_support( 'automatic-feed-links' );
}

if ( function_exists( add_theme_support( 'title-tag' ) ) ) {
	add_theme_support( 'title-tag' );
}

if ( function_exists( add_theme_support( 'menus' ) ) ) {
	add_theme_support( 'menus',
		array(
			'navigation-top' => __( 'Top Navigation Menu', 'x5' ),
			'navigation-footer' => __( 'Footer Navigation Menu', 'x5' ),
		)
	);
}


/*
 * Register menus
 */
function x5_register_menus() {
	
	register_nav_menus(
		array(
			'header-menu' => esc_html__( 'Header Menu', 'x5' ),
		)
	);
}
add_action( 'init', 'x5_register_menus' );

/*
 * Add addtional image sizes
 */
add_image_size( 'article_thumbnail', 335, 212, TRUE );


/*
* Add default sidebars
*/	
function x5_register_sidebars() {
		
	register_sidebar(
			array(
				'id'            => 'primary',
				'name'          => __( 'Primary Sidebar', 'x5' ),
				'description'   => __( 'Main content sidebar.', 'x5' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
	);
	/* Repeat register_sidebar() code for additional sidebars. */
}
add_action( 'widgets_init', 'x5_register_sidebars' );


if ( ! isset( $content_width ) ) {
	$content_width = 1210;
}


/* 
 * Load optimization
 */
// Remove Emoji icons
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


/*
 * Enqueue styles and scripts
 */
function x5_add_scripts() {

	// Header
	wp_enqueue_style( 'x5-style', get_stylesheet_uri() );

	wp_enqueue_style( 'x5-main', get_template_directory_uri() . '/css/main.css' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
	// Footer
	wp_enqueue_script( 'x5-plugins', get_template_directory_uri() . '/js/plugins.min.js', array( 'jquery' ), '04092017', true );

	wp_enqueue_script( 'x5-js', get_template_directory_uri() . '/js/main.js', array( 'jquery', 'x5-plugins' ), '04092017', true );
}
add_action( 'wp_enqueue_scripts', 'x5_add_scripts' );


/*
 * Add styles to editor
 */
function x5_add_editor_styles() {
	add_editor_style( get_template_directory_uri() . '/css/main.css' );
}
add_action( 'admin_init', 'x5_add_editor_styles' );


/*
 * Add ACF Options pages
 */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
	acf_add_options_sub_page( 'Header' );
	acf_add_options_sub_page( 'Footer' );
	acf_add_options_sub_page( 'General' );
}

/*
 * Add client defined styles to header
 */
function x5_add_customized_css() {

	?>
	
	<style>
		
		<?php
			
			// Header logo
			if ( get_field( 'x5_general_logo', 'option' ) ):
				$x5_logo = get_field( 'x5_general_logo', 'option' );
			?>
			
				.c-logo {
					width: <?php echo wp_filter_nohtml_kses( $x5_logo['width'] ); ?>px;
					height: <?php echo wp_filter_nohtml_kses( $x5_logo['height'] ); ?>px;
					background: transparent url(<?php echo wp_filter_nohtml_kses( $x5_logo['url'] ) ?>) 0 0 no-repeat;
				}
				
			<?php
		endif; 
		
		// General contact section background 
		if ( get_field( 'x5_general_contact_bg', 'option' ) ): 
		
			$x5_general_contact_bg = get_field( 'x5_general_contact_bg', 'option' ); 
			?>
			
			.c-get-in-touch {
			  background: transparent url('<?php echo wp_filter_nohtml_kses( $x5_general_contact_bg['url'] ); ?>') center center no-repeat;
			  background-size: cover;
			}
	
			<?php 
		
		endif; 

		// Home page
		if ( is_page_template( 'page-home.php' ) ) :
			
			if ( get_field ( 'x5_home_intro_bg_m' ) ) :
				$x5_home_intro_bg_m = get_field( 'x5_home_intro_bg_m' );
			
			?>
				.page-home .c-intro {
				  background: transparent url('<?php echo esc_url( $x5_home_intro_bg_m['url'] ); ?>') center 100% no-repeat;
				  background-size: cover;
				}				
		
			<?php 

			endif;

			if ( get_field ( 'x5_home_intro_bg_t' ) ) :
				$x5_home_intro_bg_t = get_field( 'x5_home_intro_bg_t' );
			
			?>
				@media screen and (min-width: 540px) {
				  .page-home .c-intro {
				    background-image: url('<?php echo esc_url( $x5_home_intro_bg_t['url'] ); ?>');
				    background-size: cover;
				  }
				}		
			<?php 

			endif;

			if ( get_field ( 'x5_home_intro_bg_d' ) ) :
				$x5_home_intro_bg_d = get_field( 'x5_home_intro_bg_d' );
			
			?>
				@media screen and (min-width: 1140px) {
				  .page-home .c-intro {
				    background-image: url('<?php echo esc_url( $x5_home_intro_bg_d['url'] ); ?>');
				    background-size: cover;
				  }
				}				
		
			<?php 

			endif;

			if ( get_field ( 'x5_home_why_img' ) ) :
				$x5_home_why_img = get_field( 'x5_home_why_img' );
			
			?>
				.page-home .c-why-us .o-holder {
				  background: transparent url('<?php echo esc_url( $x5_home_why_img['url'] ); ?>') 3px 160px no-repeat;
				  background-position: center 55px;
				  background-size: 260px auto;
				}	

				@media screen and (min-width: 768px){
					.page-home .c-why-us .o-holder {
				    background-size: 294px auto;
				  }
				}

				@media screen and (min-width: 1140px) {
					.page-home .c-why-us .o-holder {
						background-position: 3px 239px;
				    background-size: auto auto;
				  }
				}	
		
			<?php 

			endif;

			if ( get_field ( 'x5_home_testimonials_bg' ) ) :
				$x5_home_testimonials_bg = get_field( 'x5_home_testimonials_bg' );
			
			?>
				.page-home .c-testimonials .heading-holder {
				  float: left;
				  width: 100%;
				  padding: 11.625rem 0 9.0625rem;
				  background: transparent url('<?php echo esc_url( $x5_home_testimonials_bg['url'] ); ?>') 0 0 no-repeat;
				}	
		
			<?php 

			endif;
		
		endif;

		// Default page
		if ( is_page() ) :
			
			if ( get_field ( 'x5_default_intro_bg_m' ) ) :
				$x5_default_intro_bg_m = get_field( 'x5_default_intro_bg_m' );
			
			?>
				.page-default .c-intro {
				  background: transparent url('<?php echo esc_url( $x5_default_intro_bg_m['url'] ); ?>') center 100% no-repeat;
				  background-size: cover;
				}				
		
			<?php 

			endif;

			if ( get_field ( 'x5_default_intro_bg_t' ) ) :
				$x5_default_intro_bg_t = get_field( 'x5_default_intro_bg_t' );
			
			?>
				@media screen and (min-width: 768px) {
				  .page-default .c-intro {
				    background-image: url('<?php echo esc_url( $x5_default_intro_bg_t['url'] ); ?>');
				    background-size: cover;
				  }
				}		
			<?php 

			endif;

			if ( get_field ( 'x5_default_intro_bg_d' ) ) :
				$x5_default_intro_bg_d = get_field( 'x5_default_intro_bg_d' );
			
			?>
				@media screen and (min-width: 1140px) {
				  .page-default .c-intro {
				    background-image: url('<?php echo esc_url( $x5_default_intro_bg_d['url'] ); ?>');
				    background-size: cover;
				  }
				}				
		
			<?php 

			endif;
		
		endif;

		// About page
		if ( is_page_template( 'page-about.php' ) ) :
			
			if ( get_field ( 'x5_about_intro_bg_m' ) ) :
				$x5_about_intro_bg_m = get_field( 'x5_about_intro_bg_m' );
			
			?>
				.page-about .c-intro {
				  background: transparent url('<?php echo esc_url( $x5_about_intro_bg_m['url'] ); ?>') center 100% no-repeat;
				  background-size: cover;
				}				
		
			<?php 

			endif;

			if ( get_field ( 'x5_about_intro_bg_t' ) ) :
				$x5_about_intro_bg_t = get_field( 'x5_about_intro_bg_t' );
			
			?>
				@media screen and (min-width: 768px) {
				  .page-about .c-intro {
				    background-image: url('<?php echo esc_url( $x5_about_intro_bg_t['url'] ); ?>');
				    background-size: cover;
				  }
				}		
			<?php 

			endif;

			if ( get_field ( 'x5_about_intro_bg_d' ) ) :
				$x5_about_intro_bg_d = get_field( 'x5_about_intro_bg_d' );
			
			?>
				@media screen and (min-width: 1140px) {
				  .page-about .c-intro {
				    background-image: url('<?php echo esc_url( $x5_about_intro_bg_d['url'] ); ?>');
				    background-size: cover;
				  }
				}				
		
			<?php 

			endif;
		
		endif;

		// Contact page
		if ( is_page_template( 'page-contact.php' ) ) :
			
			if ( get_field ( 'x5_contact_intro_bg_m' ) ) :
				$x5_contact_intro_bg_m = get_field( 'x5_contact_intro_bg_m' );
			
			?>
				.page-contact .c-intro {
				  background: transparent url('<?php echo esc_url( $x5_contact_intro_bg_m['url'] ); ?>') center 100% no-repeat;
				  background-size: cover;
				}				
		
			<?php 

			endif;

			if ( get_field ( 'x5_contact_intro_bg_t' ) ) :
				$x5_contact_intro_bg_t = get_field( 'x5_contact_intro_bg_t' );
			
			?>
				@media screen and (min-width: 540px) {
				  .page-contact .c-intro {
				    background-image: url('<?php echo esc_url( $x5_contact_intro_bg_t['url'] ); ?>');
				    background-size: cover;
				  }
				}		
			<?php 

			endif;

			if ( get_field ( 'x5_contact_intro_bg_d' ) ) :
				$x5_contact_intro_bg_d = get_field( 'x5_contact_intro_bg_d' );
			
			?>
				@media screen and (min-width: 1140px) {
				  .page-contact .c-intro {
				    background-image: url('<?php echo esc_url( $x5_contact_intro_bg_d['url'] ); ?>');
				    background-size: cover;
				  }
				}				
		
			<?php 

			endif;
		
		endif;

		// Service page
		if ( is_page_template( 'page-service.php' ) ) :
			
			if ( get_field ( 'x5_service_intro_bg_m' ) ) :
				$x5_service_intro_bg_m = get_field( 'x5_service_intro_bg_m' );
			
			?>
				.page-service .c-intro {
				  background: transparent url('<?php echo esc_url( $x5_service_intro_bg_m['url'] ); ?>') center 100% no-repeat;
				  background-size: cover;
				}				
		
			<?php 

			endif;

			if ( get_field ( 'x5_service_intro_bg_t' ) ) :
				$x5_service_intro_bg_t = get_field( 'x5_service_intro_bg_t' );
			
			?>
				@media screen and (min-width: 768px) {
				  .page-service .c-intro {
				    background-image: url('<?php echo esc_url( $x5_service_intro_bg_t['url'] ); ?>');
				    background-size: cover;
				  }
				}		
			<?php 

			endif;

			if ( get_field ( 'x5_service_intro_bg_d' ) ) :
				$x5_service_intro_bg_d = get_field( 'x5_service_intro_bg_d' );
			
			?>
				@media screen and (min-width: 1140px) {
				  .page-service .c-intro {
				    background-image: url('<?php echo esc_url( $x5_service_intro_bg_d['url'] ); ?>');
				    background-size: cover;
				  }
				}				
		
			<?php 

			endif; 

			if ( get_field( 'x5_service_service_img' ) &&
					 get_field( 'x5_service_service_img_pos_x' ) &&
					 get_field( 'x5_service_service_img_pos_y' ) ) : 
				$x5_service_service_img = get_field( 'x5_service_service_img' );
				
			?>
				@media screen and (min-width: 1140px){
					.page-service .c-service .o-holder {
				    background: transparent url('<?php echo esc_url( $x5_service_service_img['url'] ); ?>') <?php echo wp_filter_nohtml_kses( get_field( 'x5_service_service_img_pos_x' ) ); ?>px <?php echo wp_filter_nohtml_kses( get_field( 'x5_service_service_img_pos_y' ) ); ?>px no-repeat;
				  }
		  	}
		  <?php elseif ( get_field( 'x5_service_service_img' ) ) : 
		  	$x5_service_service_img = get_field( 'x5_service_service_img' );
		  ?>

		  	@media screen and (min-width: 1140px){
					.page-service .c-service .o-holder {
				    background: transparent url('<?php echo esc_url( $x5_service_service_img['url'] ); ?>') 0 0 no-repeat;
				  }
		  	}
				
			<?php endif;
		 
		endif;

		// Blog page
		if ( is_page_template( 'page-blog.php' ) ) :
			
			if ( get_field ( 'x5_blog_intro_bg_m' ) ) :
				$x5_blog_intro_bg_m = get_field( 'x5_blog_intro_bg_m' );
			
			?>
				.page-blog .c-intro {
				  background: transparent url('<?php echo esc_url( $x5_blog_intro_bg_m['url'] ); ?>') center 100% no-repeat;
				  background-size: cover;
				}				
		
			<?php 

			endif;

			if ( get_field ( 'x5_blog_intro_bg_t' ) ) :
				$x5_blog_intro_bg_t = get_field( 'x5_blog_intro_bg_t' );
			
			?>
				@media screen and (min-width: 768px) {
				  .page-blog .c-intro {
				    background-image: url('<?php echo esc_url( $x5_blog_intro_bg_t['url'] ); ?>');
				    background-size: cover;
				  }
				}		
			<?php 

			endif;

			if ( get_field ( 'x5_blog_intro_bg_d' ) ) :
				$x5_blog_intro_bg_d = get_field( 'x5_blog_intro_bg_d' );
			
			?>
				@media screen and (min-width: 1140px) {
				  .page-blog .c-intro {
				    background-image: url('<?php echo esc_url( $x5_blog_intro_bg_d['url'] ); ?>');
				    background-size: cover;
				  }
				}				
		
			<?php 

			endif;
		
		endif;
		
		?>
			
	</style>
	
	<?php
}
add_action( 'wp_head', 'x5_add_customized_css' );


/*
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';


/*
 * Sanitize image
 */
function x5_sanitize_image( $input ) {
	return esc_url( $input );
}

/*
 * Display breadcrumbs
 *
 * $items - repeater field id
 * $item - menu item id
 *
 */
function x5_show_breadcrumbs( $items, $item ){
	
	if ( have_rows( $items ) ): ?>
	
		<nav class="c-breadcrumbs">
		
			<div class="o-container">
		
				<ul>
	
				<?php while( have_rows( $items ) ) : the_row(); ?>
					
					<?php if ( get_sub_field( $item ) ): 
						$x5_breadcrumbs_item = get_sub_field( $item );
					?>
						<li><a href="<?php echo esc_url( $x5_breadcrumbs_item['url'] ); ?>"><?php echo esc_html( $x5_breadcrumbs_item['title'] ); ?></a></li>
					<?php endif; ?>					
	
				<?php endwhile; ?>

				</ul>

			</div>
			<!-- / o-container -->
	
		</nav>
		<!-- c-breadcrumbs -->
	
	<?php endif;
}

/*
 * Display intro section (all pages, except Home)
 *
 * $heading - repeater field id
 * $desc - menu item id
 *
 */
function x5_show_intro_section( $heading, $desc ){

	if ( get_field( $heading ) ||
			 get_field( $desc ) ): ?>
	
	<section class="c-intro">
		
		<div class="o-container">
			
			<?php if ( get_field( $heading ) ): ?>
				<h2 class="heading"><?php echo esc_html( get_field( $heading ) ); ?></h2>
			<?php endif; ?>
			
			<?php if ( get_field( $desc ) ): ?>
				<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( $desc ) ) ); ?></div>
			<?php endif; ?>

		</div>
		<!-- / o-container -->

	</section>
	<!-- / c-intro -->

	<?php endif;
}

/*
 * Add shortcodes
 */
// [text_with_long_underline][/text_with_long_underline]
function x5_add_shortcode_long_underline( $attr, $content = null ) {
	return '<span class="first-part">'. $content .'</span>';
}

// [text_with_short_underline][/text_with_short_underline]
function x5_add_shortcode_short_underline( $attr, $content = null ) {
	return '<span class="second-part">'. $content .'</span>';
}

// [bubble][/bubble]
function x5_add_shortcode_bubble( $attr, $content = null ) {
	return '<span class="shortcode-bubble">'. $content .'</span>';
}

/*
 * Register shortcodes
 */
function x5_register_shortcodes(){
	add_shortcode( 'text_with_long_underline', 'x5_add_shortcode_long_underline' );
	add_shortcode( 'text_with_short_underline', 'x5_add_shortcode_short_underline' );
	add_shortcode( 'bubble', 'x5_add_shortcode_bubble' );
}
add_action( 'init', 'x5_register_shortcodes');
