/* ==========================================================================

    Project: TechHelp
    Last updated: August 2017

   ========================================================================== */

(function($) {

	'use strict';
	
	
	var Main = {
	
	  /**
	   * Init function
	   */
	  init: function() {
	    Main.initMenu();
	    Main.initSlideshow();
	    Main.initReadMore();
	    Main.addPaginationDots();
	    Main.onGravityFormSubmit();
	  },
	
	  /*
		 * Init menu
		 */
		initMenu: function(){
			
			$('.menu-item-has-children').append('<div class="c-nav__open-menu-btn js-nav__open-menu-btn"></div>');
			
			$('.sub-menu, .js-nav__menu').velocity('slideUp', { duration: 0 });
			
			$('.js-nav__btn').click(function(){
				var isOpen = $('.js-nav__menu').is(':visible'),direction = isOpen ? 'slideUp' : 'slideDown';
				$('.js-nav__menu').velocity(direction, { duration: 300, easing: 'ease' });
			});
		
			$('.js-nav__open-menu-btn').click(function(){
				var submenu = $(this).parent().find('.sub-menu');
				
				if($(submenu).hasClass('toggled')){
					$(submenu).velocity('slideUp', { duration: 300, easing: 'ease' });
					$(submenu).removeClass('toggled');
				
				} else {
					$(submenu).velocity('slideDown', { duration: 300, easing: 'ease' });
					$(submenu).addClass('toggled');
				}
				
				$(this).toggleClass('close');
			});	
			
			$('.js-nav__menu a[href*="#"]').parent().removeClass('current-menu-item');		
		},
		
		/*
		 * Init slideshow
		 */
		initSlideshow: function(){
			$('.royalSlider').royalSlider({
	    	autoHeight: true,
		    arrowsNav: true,
		    fadeinLoadedSlide: false,
		    controlNavigationSpacing: 0,
		    controlNavigation: 'bullets',
		    imageScaleMode: 'none',
		    imageAlignCenter: false,
		    loop: false,
		    loopRewind: false,
		    keyboardNavEnabled: false,
		    usePreloader: false
	    }); 
		},

		/*
	   * Init ReadMore functionality
	   */
	  initReadMore: function() {

	    $('.js-btn-more').click(function() {
	      var readMoreText = 'read more';
	      var readLessText = 'read less';
	      var $shortElem = $(this).parent().find(".text-short");
	      var $fullElem = $(this).parent().find(".text-full");

	      if ($shortElem.is(":visible")) {
	        $shortElem.hide();
	        $fullElem.show();
	        $(this).text(readLessText).addClass('opened');

	      } else {
	        $shortElem.show();
	        $fullElem.hide();
	        $(this).text(readMoreText).removeClass('opened');
	      }
	    });
	  },

	  /*
		 * Add pagination dots to WordPress default HTML code
		 */
		addPaginationDots: function(){
			$('.page-blog .pagination .dots').html('<span class="dot"></span><span class="dot"></span><span class="dot"></span>');
		},

		/*
		 * On GravityForms submit
		 */
		onGravityFormSubmit: function(){
			$(document).bind('gform_confirmation_loaded', function(event, formId){
				$('body').addClass('gravity-form-submitted');
			});
		} 
	};
	
	document.addEventListener('DOMContentLoaded', function() {
		Main.init();
	});
})( jQuery );