<?php
/**
 * X5: Front Page
 *
 * Can be used for Front Page display
 * see: http://codex.wordpress.org/Creating_a_Static_Front_Page
 *
 * @package WordPress
 * @subpackage X5
 */
get_header();
?>

Front page template // overwrites display of Home page.
