<?php

/*
 * X5: Page
 *
 * @package WordPress
 * @subpackage X5
 */
get_header();
the_post();

?>
	
	<?php x5_show_intro_section( 'x5_default_intro_heading', 'x5_default_intro_desc' ); ?>
	
	<?php x5_show_breadcrumbs( 'x5_default_breadcrumbs', 'x5_default_breadcrumbs_item' ); ?>		
	
	<section class="c-content">

    <div class="o-container">

      <div class="entry-content">
				
				<?php the_content(); ?>

      </div>
      <!-- entry-content -->

    </div>
    <!-- / o-container -->

  </section>
  <!-- c-content -->

<?php get_footer();
