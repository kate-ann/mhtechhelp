
<?php if ( get_field( 'x5_general_contact_heading', 'option' ) ||
					 get_field( 'x5_general_contact_desc', 'option' ) ): ?>
	
	<section class="c-get-in-touch">
			
		<div class="o-container">
			
			<div class="o-halves">

				<div class="o-halves__half">

					<?php if ( get_field( 'x5_general_contact_heading', 'option' ) ): ?>
						<h5 class="heading"><?php echo esc_html( get_field( 'x5_general_contact_heading', 'option' ) ); ?></h5>
					<?php endif; ?>
					
					<?php if ( get_field( 'x5_general_contact_desc', 'option' ) ): ?>
						<p class="desc"><?php echo esc_html( get_field( 'x5_general_contact_desc', 'option' ) ); ?></p>
					<?php endif; ?>
					
				</div>
				<!-- o-halves__half -->
				
				<div class="o-halves__half">
					
					<div class="c-contact-form">
						
						<?php gravity_form( 1, false, false, false, '', true, false ); ?>
							
					</div>
					<!-- / c-contact-form -->
				
				</div>
				<!-- o-halves__half -->

			</div>
			<!-- o-halves -->

		</div>
		<!-- / o-container -->
			
	</section>
	<!-- / get-in-touch -->

<?php endif; ?>

