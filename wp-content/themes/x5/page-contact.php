<?php
/**
* Template Name: Contact
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php x5_show_intro_section( 'x5_contact_intro_heading', 'x5_contact_intro_desc' ); ?>

<?php x5_show_breadcrumbs( 'x5_contact_breadcrumbs', 'x5_contact_breadcrumbs_item' ); ?>

<?php if ( get_field( 'x5_contact_contact_tel_text' ) ||
					 get_field( 'x5_contact_contact_tel' ) ||
					 get_field( 'x5_contact_contact_heading' ) ||
					 get_field( 'x5_contact_contact_desc' ) ) : ?>

	<section class="c-about">
		
		<div class="o-container">
			
			<?php if ( get_field( 'x5_contact_contact_tel_text' ) ||
					 			 get_field( 'x5_contact_contact_tel' ) ) : ?>
				
				<div class="vcard">

					<?php if ( get_field( 'x5_contact_contact_tel_text' ) ): ?>
						<p class="text"><?php echo esc_html( get_field( 'x5_contact_contact_tel_text' ) ); ?></p>
					<?php endif; ?>
					
					<?php if ( get_field( 'x5_contact_contact_tel' ) &&
										 get_field( 'x5_contact_contact_tel_link' ) ):
					 	$x5_contact_contact_tel = get_field ( 'x5_contact_contact_tel' );
					 	$x5_contact_contact_tel_link = get_field ( 'x5_contact_contact_tel_link' ); ?>
					 	
					 	<a href="<?php echo esc_url( 'tel:' . $x5_contact_contact_tel_link ); ?>" class="tel"><?php echo esc_html( $x5_contact_contact_tel ); ?></a>
					 	<span class="icon"></span>
					 
					 <?php endif; ?> 

				</div>
				<!-- vcard  -->

			<?php endif; ?>
			
			<?php if ( get_field( 'x5_contact_contact_heading' ) || 
					 			 get_field( 'x5_contact_contact_desc' ) ): ?>
				
				<div class="info">
					
					<?php if ( get_field( 'x5_contact_contact_heading' ) ): ?>
						<h3 class="heading"><?php echo esc_html( get_field( 'x5_contact_contact_heading' ) ); ?></h3>
					<?php endif; ?>
					
					<?php if ( get_field( 'x5_contact_contact_desc' ) ): ?>
						<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_contact_contact_desc' ) ) ); ?></div>
					<?php endif; ?>

					<div class="c-contact-form">
					
						<?php gravity_form( 2, false, false, false, '', true, false ); ?>

					</div>
					<!-- c-contact-form -->

				</div>
				<!-- info -->

			<?php endif; ?>
		
		</div>
		<!-- / o-container -->

	</section>
	<!-- c-about -->	

<?php endif; ?>

<?php get_footer();
