<?php
/**
* Template Name: About
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php x5_show_intro_section( 'x5_about_intro_heading', 'x5_about_intro_desc' ); ?>

<?php x5_show_breadcrumbs( 'x5_about_breadcrumbs', 'x5_about_breadcrumbs_item' ); ?>		

<?php if ( get_field( 'x5_about_about_img' ) ||
					 get_field( 'x5_about_about_heading' ) ||
					 get_field( 'x5_about_about_desc' ) ): ?>
	
	<section class="c-about">
		
		<div class="o-container">
			
			<div class="o-holder">
				
				<?php if ( get_field( 'x5_about_about_img' ) ): 
					$x5_about_about_img = get_field( 'x5_about_about_img' ); ?>
  							
					<img class="img" src="<?php echo esc_url( $x5_about_about_img['url'] ); ?>" alt="" >
				<?php endif; ?>
				
		
				<div class="info">
					
					<?php if ( get_field( 'x5_about_about_heading' ) ): ?>
						<h3 class="heading"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_about_about_heading' ) ) ); ?></h3>
					<?php endif; ?>
					
					<?php if ( get_field( 'x5_about_about_desc' ) ): ?>
						<div class="desc entry-content">
							<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_about_about_desc' ) ) ); ?>
						</div>
						<!-- desc -->
					<?php endif; ?>					

				</div>
				<!-- info -->
			
			</div>
			<!-- o-holder -->

		</div>
		<!-- / o-container -->

	</section>
	<!-- c-about -->

<?php endif; ?>

<?php get_template_part( 'partials/contact', 'section' ); ?>

<?php get_footer();
