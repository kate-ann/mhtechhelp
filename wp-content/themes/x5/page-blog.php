<?php
/**
* Template Name: Blog
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php x5_show_intro_section( 'x5_blog_intro_heading', 'x5_blog_intro_desc' ); ?>

<?php x5_show_breadcrumbs( 'x5_blog_breadcrumbs', 'x5_blog_breadcrumbs_item' ); ?>

<?php
	
global $wp_query;
$original_query = $wp_query;

$paged = 1;
if ( get_query_var('paged') ) {
	$paged = get_query_var('paged');
}

$custom_post_type = new WP_Query(
  array(
    'post_type' => 'article',
    'orderby' => 'date',
    'order' => 'DESC',
    'posts_per_page' => 6,
    'paged' => $paged,
  )
);

$wp_query = $custom_post_type;

if( $custom_post_type->have_posts() ) : ?>

	<section class="c-blogs">

		<div class="o-container">
			
			<div class="o-holder">
				
				<div class="o-thirds">
				
					<?php while( $custom_post_type->have_posts() ): $custom_post_type->the_post(); ?>
	          
	          <div class="o-thirds__third">
								
							<?php if ( get_field( 'x5_article_img' ) &&
												 get_field( 'x5_article_full_link' ) ):
								$x5_article_img = get_field( 'x5_article_img' );
								$x5_article_img_id = $x5_article_img['ID'];
								$x5_article_img_custom_size = wp_get_attachment_image( $x5_article_img_id, 'article_thumbnail' );
								$x5_article_full_link = get_field( 'x5_article_full_link' ); ?>
								
								<a href="<?php echo esc_url( $x5_article_full_link['url'] ); ?>" class="img"><?php echo wp_kses_post( force_balance_tags( $x5_article_img_custom_size ) ); ?></a>

							<?php endif; ?>
						
							<?php if ( get_field( 'x5_article_heading' ) &&
												 get_field( 'x5_article_full_link' ) ):
								$x5_article_full_link = get_field( 'x5_article_full_link' ); ?>
								
								<h4 class="subheading"><a href="<?php echo esc_url( $x5_article_full_link['url'] ); ?>"><?php echo esc_html( get_field( 'x5_article_heading' ) ); ?></a></h4>
							<?php endif; ?>
							
							<?php if ( get_field( 'x5_article_excerpt' ) ||
												 get_field( 'x5_article_desc' ) ): ?>
							
								<div class="text js-expand">
									<?php if ( get_field( 'x5_article_excerpt' ) ): ?>
										<div class="text-short"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_article_excerpt' ) ) ); ?></div>
									<?php endif; ?>
									
									<?php if ( get_field( 'x5_article_desc' ) ): ?>
										<div class="text-full"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_article_desc' ) ) ); ?></div>

										<span class="c-link js-btn-more"><?php echo esc_html( 'read more' ); ?></span>
									<?php endif; ?>	               
								</div>
								<!-- text -->

							<?php endif; ?>

					</div>
					<!-- / o-thirds__third -->	

	    	<?php endwhile; ?>

	    </div>
			<!-- o-holder -->	

			<?php the_posts_pagination(); ?>	

		</div>
		<!-- / o-container -->

	</section>
	<!-- c-blogs -->
        
<?php endif; ?>

<?php 
	wp_reset_postdata();
	$wp_query = $original_query; 
?>


<?php get_template_part( 'partials/contact', 'section' ); ?>

<?php get_footer();
