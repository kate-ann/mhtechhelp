<?php
/**
* Template Name: Article
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php x5_show_intro_section( 'x5_blog_intro_heading', 'x5_blog_intro_desc' ); ?>

<?php x5_show_breadcrumbs( 'x5_blog_breadcrumbs', 'x5_blog_breadcrumbs_item' ); ?>

<?php
	
	// $current_page = 1;
	
	// if ( get_query_var( 'paged' ) ) { 
	// 	$current_page = get_query_var('paged');
	// } elseif ( get_query_var('page') ) {
	// 	$current_page = get_query_var('page');
	// }
	
	// Store original posts query in $original_query

    global $wp_query;
    $original_query = $wp_query;
    
    $paged = ( get_query_var('page') ) ? get_query_var('page') :1;
    $custom_post_type = new WP_Query(
        array(
          'post_type' => 'article',
          'orderby' => 'date',
          'order' => 'DESC',
          'posts_per_page' => 6,
          'paged' => $paged,
        )
    );

    $wp_query = $custom_post_type;

    if( $custom_post_type->have_posts() ) :
        while( $custom_post_type->have_posts() ): $custom_post_type->the_post(); ?>
            <h1><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h1>
            <p> <?php the_modified_date(); echo ", "; the_modified_time() ?></p>
            <p> <?php the_excerpt() ?></p>
        <?php endwhile;
        the_posts_pagination();
    endif;
    wp_reset_postdata();

    ?>
<?php get_template_part( 'partials/contact', 'section' ); ?>

<?php get_footer();
