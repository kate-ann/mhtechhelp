<?php
/**
* Template Name: Home
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php if ( get_field( 'x5_home_intro_heading' ) ||
			 		 get_field( 'x5_home_intro_desc' ) ): ?>
	
	<section class="c-intro">
		
		<div class="o-container">
			
			<?php if ( get_field( 'x5_home_intro_heading' ) ): ?>
				<h2 class="heading"><?php echo esc_html( get_field( 'x5_home_intro_heading' ) ); ?></h2>
			<?php endif; ?>
			
			<?php if ( get_field( 'x5_home_intro_desc' ) ): ?>
				<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_intro_desc' ) ) ); ?></div>
			<?php endif; ?>
			
			<?php if ( get_field( 'x5_home_intro_cbtnl' ) &&
								 get_field( 'x5_home_intro_cbtnl_text' ) ): ?>
				
				<a href="<?php echo esc_url( get_field( 'x5_home_intro_cbtnl' ) ); ?>" class="c-btn"><?php echo esc_html( get_field( 'x5_home_intro_cbtnl_text' ) ); ?></a>
			
			<?php endif; ?>
			
			<?php if ( get_field( 'x5_home_intro_ctext' ) ): ?>
				<div class="message"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_intro_ctext' ) ) ); ?></div>
			<?php endif; ?>			

		</div>
		<!-- / o-container -->
		
		<?php if ( get_field( 'x5_home_intro_ns_heading' ) ): ?>
			<p class="subtitle"><?php echo esc_html( get_field( 'x5_home_intro_ns_heading' ) ); ?></p>
		<?php endif; ?>			

	</section>
	<!-- / c-intro -->

<?php endif; ?>

<?php if ( get_field( 'x5_home_services_heading' ) ||
					 get_field( 'x5_home_services_desc' ) ): ?>
	
	<section id="services" class="c-services">
		
		<div class="o-container">
			
			<?php if ( get_field( 'x5_home_services_heading' ) ): ?>
				<h3 class="heading"><?php echo esc_html( get_field( 'x5_home_services_heading' ) ); ?></h3>
			<?php endif; ?>
			
			<?php if ( get_field( 'x5_home_services_desc' ) ): ?>
				<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_services_desc' ) ) ); ?></div>
			<?php endif; ?>
			
			<?php if ( have_rows( 'x5_home_services_services' ) ): ?>
			
				<div class="o-holder">

					<div class="o-halves">
			
						<?php while( have_rows( 'x5_home_services_services' ) ) : the_row(); ?>
				
							<div class="c-service o-halves__half computer">
								
								<?php 
									$x5_home_services_service_icon = '';
									$x5_home_services_service_icon_2x = '';
									$x5_home_services_service_rbtnl = '';

									if ( get_sub_field( 'x5_home_services_service_icon' ) ){
										$x5_home_services_service_icon = get_sub_field( 'x5_home_services_service_icon' );
									}
									if ( get_sub_field( 'x5_home_services_service_icon_2x' ) ){
										$x5_home_services_service_icon_2x = get_sub_field( 'x5_home_services_service_icon_2x' );
									}

									if ( get_sub_field( 'x5_home_services_service_rbtnl' ) ){
										$x5_home_services_service_rbtnl = get_sub_field( 'x5_home_services_service_rbtnl' );
									}
								?>
								
								<a href="<?php echo esc_url( $x5_home_services_service_rbtnl['url'] ); ?>" class="img">
									<picture>
										<source srcset="<?php echo esc_url( $x5_home_services_service_icon['url'] ); ?> 1x, <?php echo esc_url( $x5_home_services_service_icon_2x['url'] ); ?> 2x" media="(min-width: 0px)">
										<img srcset="<?php echo esc_url( $x5_home_services_service_icon['url'] ); ?>" alt="">
									</picture>
								</a>
						
								<?php if ( get_sub_field( 'x5_home_services_service_heading' ) ||
													 get_sub_field( 'x5_home_services_service_desc' ) ): ?>
									
									<div class="info">
										<?php if ( get_sub_field( 'x5_home_services_service_title' ) ): ?>
											<h3 class="c-service__heading"><a href="<?php echo esc_url( $x5_home_services_service_rbtnl['url'] ); ?>"><?php echo esc_html( get_sub_field( 'x5_home_services_service_title' ) ); ?></a></h3>
										<?php endif; ?>
										
										<?php if ( get_sub_field( 'x5_home_services_service_desc' ) ): ?>
											
											<p class="c-service__text"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_home_services_service_desc' ) ) ); ?>
											
												<?php if ( get_sub_field( 'x5_home_services_service_rbtnl' ) ):
														$x5_home_services_service_rbtnl = get_sub_field( 'x5_home_services_service_rbtnl' ); ?>
													<a href="<?php echo esc_url( $x5_home_services_service_rbtnl['url'] ); ?>"><?php echo esc_html( $x5_home_services_service_rbtnl['title'] ); ?></a>
												
												<?php endif; ?>
												
											</p>

										<?php endif; ?>
										
									</div>
									<!-- info -->

								<?php endif; ?>								

							</div>
							<!-- / c-service -->

						<?php endwhile; ?>
				
					</div>
					<!-- / o-halves -->

				</div>
				<!-- o-holder -->
			
			<?php endif; ?>

		</div>
		<!-- / o-container -->
	
	</section>
	<!-- / c-services -->

<?php endif; ?>

<?php if ( get_field( 'x5_home_about_heading' ) ||
					 get_field( 'x5_home_about_desc' ) ): ?>
	
	<section class="c-about">
		
		<div class="o-container">
			
			<?php if ( get_field( 'x5_home_about_img' ) &&
								 get_field( 'x5_home_about_rbtnl' ) ): 
				$x5_home_about_img = get_field( 'x5_home_about_img' );
				$x5_home_about_rbtnl = get_field( 'x5_home_about_rbtnl' );?>
				
				<a href="<?php echo esc_url( $x5_home_about_rbtnl['url'] ); ?>" class="headshot"><img src="<?php echo esc_url( $x5_home_about_img['url'] ); ?>" alt="<?php echo esc_attr( $x5_home_about_img['alt'] ); ?>"></a>
			
			<?php elseif ( get_field( 'x5_home_about_img' ) ):
				$x5_home_about_img = get_field( 'x5_home_about_img' ); ?>
				
				<img class="headshot" src="<?php echo esc_url( $x5_home_about_img['url'] ); ?>" alt="<?php echo esc_attr( $x5_home_about_img['alt'] ); ?>">
			<?php endif; ?>	
			
			<?php if ( get_field( 'x5_home_about_heading' ) ||
								 get_field( 'x5_home_about_desc' ) ||
								 get_field( 'x5_home_about_rbtnl' ) ): ?>
				
				<div class="info">
				
					<?php if ( get_field( 'x5_home_about_heading' ) ): ?>
						<h3 class="heading"><?php echo esc_html( get_field( 'x5_home_about_heading' ) ); ?></h3>
					<?php endif; ?>

					<?php if ( get_field( 'x5_home_about_desc' ) ): ?>
						<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_about_desc' ) ) ); ?></div>
					<?php endif; ?>
			
					<?php if ( get_field( 'x5_home_about_rbtnl' ) ):
						$x5_home_about_rbtnl = get_field( 'x5_home_about_rbtnl' ); ?>
						<a href="<?php echo esc_url( $x5_home_about_rbtnl['url'] ); ?>" class="c-link"><?php echo esc_html( $x5_home_about_rbtnl['title'] ); ?> <span class="icon-arrow-right"></span></a>
					<?php endif; ?>

				</div>
				<!-- info -->

			<?php endif; ?>

		</div>
		<!-- / o-container -->
	
	</section>
	<!-- / c-about -->

<?php endif; ?>

<?php if ( get_field( 'x5_home_why_heading' ) ||
								 get_field( 'x5_home_why_desc' ) ||
								 get_field( 'x5_home_why_features' ) ): ?>
				
	<section class="c-why-us">

		<div class="o-container">
			
			<div class="o-holder">
				
				<div class="info">
					
					<?php if ( get_field( 'x5_home_why_heading' ) ): ?>
						<h4 class="heading"><?php echo esc_html( get_field( 'x5_home_why_heading' ) ); ?></h4>
					<?php endif; ?>
					
					<?php if ( get_field( 'x5_home_why_desc' ) ): ?>
						<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_why_desc' ) ) ); ?></div>
					<?php endif; ?>
					
					<?php if ( have_rows( 'x5_home_why_features' ) ): ?>
						
						
						
						<ul class="items">
					
							<?php while( have_rows( 'x5_home_why_features' ) ) : the_row(); ?>
								
								<?php 
									$x5_home_why_feature_icon_class = '';

									if ( get_sub_field( 'x5_home_why_feature_icon' ) ):
								
										$x5_home_why_feature_icon_type = get_sub_field( 'x5_home_why_feature_icon' );

										switch ( $x5_home_why_feature_icon_type ) {
											case 'Map':
												$x5_home_why_feature_icon_class = 'map';
												break;
											case 'Bulb':
												$x5_home_why_feature_icon_class = 'bulb';
												break;
											case 'Smiley':
												$x5_home_why_feature_icon_class = 'smiley';
												break;
										} 
									endif; 
								?>

								<?php if ( $x5_home_why_feature_icon_class == '' ): ?>
									<li>
								<?php else: ?>
									<li class="<?php echo esc_attr( $x5_home_why_feature_icon_class ); ?>">
								<?php endif ?>

									<?php if ( get_sub_field( 'x5_home_why_feature_title' ) ): ?>
										<h5 class="subheading"><?php echo esc_html( get_sub_field( 'x5_home_why_feature_title' ) ); ?></h5>
									<?php endif; ?>
									
									<?php if ( get_sub_field( 'x5_home_why_feature_desc' ) ): ?>
										<div class="text"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_home_why_feature_desc' ) ) ); ?></div>
									<?php endif; ?>
								
									<span class="icon"></span>
								</li>	
					
							<?php endwhile; ?>
					
						</ul>
						<!-- / items -->
					
					<?php endif; ?>
				
				</div>
				<!-- / o-info -->

			</div>
			<!-- o-holder -->
			
		</div>
		<!-- / o-container -->
		
	</section>
	<!-- / c-why-us -->

<?php endif; ?>

<?php if ( get_field( 'x5_home_testimonials_heading' ) ||
					 have_rows( 'x5_home_testimonials_slider' ) ): ?>
	
	<section class="c-testimonials">
		
		<?php if ( get_field( 'x5_home_testimonials_heading' ) ): ?>
			<div class="heading-holder">
			<h3 class="heading"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_testimonials_heading' ) ) ); ?></h3>
		</div>
		<!-- heading-holder -->
		<?php endif; ?>

		<?php if ( have_rows( 'x5_home_testimonials_slider' ) ): ?>
		
			<div class="o-container">
	
				<div class="slider-holder">
				
					<div class="slider royalSlider rsDefault">
		
						<?php while( have_rows( 'x5_home_testimonials_slider' ) ) : the_row(); ?>
				
							<div class="slide">
								<?php if ( get_sub_field( 'x5_home_testimonials_testimonial' ) ): ?>
									<div class="testimonial"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_home_testimonials_testimonial' ) ) ); ?></div>
								<?php endif; ?>
								
								<?php if ( get_sub_field( 'x5_home_testimonials_reply' ) ): ?>
									<div class="reply"><?php echo wp_kses_post( force_balance_tags( get_sub_field( 'x5_home_testimonials_reply' ) ) ); ?></div>
								<?php endif; ?>
								
								<?php if ( get_sub_field( 'x5_home_testimonials_author' ) ||
													 get_sub_field( 'x5_home_testimonials_author_img' ) ): ?>
									<div class="author">
										<?php if ( get_sub_field( 'x5_home_testimonials_author' ) ): ?>
											<span class="name"><?php echo esc_html( get_sub_field( 'x5_home_testimonials_author' ) ); ?></span>
										<?php endif; ?>
										
										<?php if ( get_sub_field( 'x5_home_testimonials_author_img' ) ):
													$x5_home_testimonials_author_img = get_sub_field( 'x5_home_testimonials_author_img' );

										?>

											<span class="img">
												<img src="<?php echo esc_url( $x5_home_testimonials_author_img['url'] ); ?>" alt="" />
											</span>
											
										<?php endif; ?>
										
									</div>
								<!-- / author -->
								<?php endif; ?>
								
							</div>
							<!-- / slide -->
				
						<?php endwhile; ?>
		
					</div>					
					<!-- slider -->

				</div>
				<!-- slider-holder -->

			</div>
			<!-- / o-container -->
		
		<?php endif; ?>
		
		<?php if ( get_field( 'x5_home_cta_heading' ) ||
							 get_field( 'x5_home_cta_desc' ) ||
							 get_field( 'x5_home_cta_cbtnl_text' ) ||
							 get_field( 'x5_home_cta_cbtnl' ) ||
							 get_field( 'x5_home_cta_contact_text' ) ): ?>
			
			<div class="c-cta">
			
				<div class="o-container">
					
					<?php if ( get_field( 'x5_home_cta_heading' ) ||
										 get_field( 'x5_home_cta_desc' ) ): ?>
						<div class="info">

							<?php if ( get_field( 'x5_home_cta_heading' ) ): ?>
								<h5 class="heading"><?php echo esc_html( get_field( 'x5_home_cta_heading' ) ); ?></h5>
							<?php endif; ?>
							
							<?php if ( get_field( 'x5_home_cta_desc' ) ): ?>
								<div class="desc"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_cta_desc' ) ) ); ?></div class="desc">
							<?php endif; ?>
							
						</div>
						<!-- info -->
					
					<?php endif; ?>

					<?php if ( get_field( 'x5_home_cta_cbtnl' ) ||
										 get_field( 'x5_home_cta_cbtnl_text' ) ||
										 get_field( 'x5_home_cta_contact_text' ) ): ?>
						
						<div class="contact">

							<?php if ( get_field( 'x5_home_cta_cbtnl' ) &&
												 get_field( 'x5_home_cta_cbtnl_text' ) ): ?>
								
								<a href="<?php echo esc_url( get_field( 'x5_home_cta_cbtnl' ) ); ?>" class="c-btn"><?php echo esc_html( get_field( 'x5_home_cta_cbtnl_text' ) ); ?></a>

							<?php endif; ?>

							<?php if ( get_field( 'x5_home_cta_contact_text' ) ): ?>
								<div class="message"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_home_cta_contact_text' ) ) ); ?></div>
							<?php endif; ?>
							
						</div>	
						<!-- contact -->

					<?php endif; ?>

				</div>
				<!-- / o-container -->
		
			</div>
			<!-- / c-cpt -->

		<?php endif; ?>
		
		
	</section>
	<!-- / testimonials -->

<?php endif; ?>

<?php
	$articles_args = [
		'post_type' => 'article',
		'posts_per_page' => 3,
		'order' => 'DESC',
		'orderby' => 'date',
	];
?>

<?php	$articles_query = new WP_Query( $articles_args ); ?>

<?php if ( $articles_query->have_posts() ) : ?>
	
	<section class="c-blogs">
		
		<div class="o-container">
			
			<div class="o-holder">

				<?php if ( get_field( 'x5_home_blog_heading' ) ): ?>
					<h5 class="heading"><?php echo esc_html( get_field( 'x5_home_blog_heading' ) ); ?></h5>
				<?php endif; ?>
				
				<div class="o-thirds">
		
					<?php while ( $articles_query->have_posts() ) : $articles_query->the_post(); ?>
						
						<div class="o-thirds__third">
							
							<?php if ( get_field( 'x5_article_img' ) &&
												 get_field( 'x5_article_full_link' ) ):
								$x5_article_img = get_field( 'x5_article_img' );
								$x5_article_img_id = $x5_article_img['ID'];
								$x5_article_img_custom_size = wp_get_attachment_image( $x5_article_img_id, 'article_thumbnail' ); 
								$x5_article_full_link = get_field( 'x5_article_full_link' );?>
								
								<a href="<?php echo esc_url( $x5_article_full_link['url'] ); ?>" class="img"><?php echo wp_kses_post( force_balance_tags( $x5_article_img_custom_size ) ); ?></a>

							<?php endif; ?>
							
							<?php if ( get_field( 'x5_article_heading' ) &&
												 get_field( 'x5_article_full_link' ) ):

										$x5_article_full_link = get_field( 'x5_article_full_link' );
								?>
								
								<h4 class="subheading"><a href="<?php echo esc_url( $x5_article_full_link['url'] ); ?>"><?php echo esc_html( get_field( 'x5_article_heading' ) ); ?></a></h4>
							
							<?php endif; ?>
							
							<?php if ( get_field( 'x5_article_excerpt' ) ||
												 get_field( 'x5_article_desc' ) ): ?>
								
								<div class="text js-expand">
									<?php if ( get_field( 'x5_article_excerpt' ) ): ?>
										<div class="text-short"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_article_excerpt' ) ) ); ?></div>
									<?php endif; ?>
									
									<?php if ( get_field( 'x5_article_desc' ) ): ?>
										<div class="text-full"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_article_desc' ) ) ); ?></div>

										<span class="c-link js-btn-more"><?php echo esc_html( 'read more' ); ?></span>
									<?php endif; ?>	              
		              
								</div>

							<?php endif; ?>
							
						
						</div>
						<!-- / o-thirds__third -->
						
					<?php endwhile; ?>
			
				</div>
				<!-- / o-thirds -->

			</div>
			<!-- o-holder -->		

		</div>
		<!-- / o-container -->

	</section>
	<!-- c-blogs -->
	
	<?php wp_reset_postdata(); ?>
			
<?php else : ?>
	<p><?php _e( 'Sorry, no projects matched your criteria.', 'x5' ); ?></p>
<?php endif; ?>	


<?php get_template_part( 'partials/contact', 'section' ); ?>

<?php get_footer();
