<?php
/**
* Template Name: Service
*
* @package WordPress
* @subpackage X5
*/
get_header();
?>

<?php x5_show_intro_section( 'x5_service_intro_heading', 'x5_service_intro_desc' ); ?>

<?php x5_show_breadcrumbs( 'x5_service_breadcrumbs', 'x5_service_breadcrumbs_item' ); ?>		

<?php if ( get_field( 'x5_service_service_img' ) ||
					 get_field( 'x5_service_service_heading' ) ||
					 get_field( 'x5_service_service_desc' ) ): ?>
	
	<section class="c-service">
		
		<div class="o-container">
			
			<div class="o-holder">
				
				<?php if ( get_field( 'x5_service_service_img' ) ): 
					$x5_service_service_img = get_field( 'x5_service_service_img' ); ?>
  							
					<img class="img" src="<?php echo esc_url( $x5_service_service_img['url'] ); ?>" alt="" >
				<?php endif; ?>
				
		
				<div class="info">
					
					<?php if ( get_field( 'x5_service_service_heading' ) ): ?>
						<h3 class="heading"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_service_service_heading' ) ) ); ?></h3>
					<?php endif; ?>
					
					<?php if ( get_field( 'x5_service_service_desc' ) ): ?>
						<div class="desc entry-content">
							<?php echo wp_kses_post( force_balance_tags( get_field( 'x5_service_service_desc' ) ) ); ?>
						</div>
						<!-- desc -->
					<?php endif; ?>					
					
					<?php if ( get_field( 'x5_service_service_cta_btnt' ) ||
										 get_field( 'x5_service_service_cta_btnl' ) ||
										 get_field( 'x5_service_service_arrow_show' ) ): ?>
						
						<div class="cta-holder">
							
							<?php if ( get_field( 'x5_service_service_cta_btnt' ) &&
												 get_field( 'x5_service_service_cta_btnl' ) ): ?>
								
								<a class="c-btn c-btn--no-fill" href="<?php echo esc_url( 'tel:' . get_field ( 'x5_service_service_cta_btnl' ) ); ?>"><?php echo esc_html( get_field( 'x5_service_service_cta_btnt' ) ); ?></a>
							
							<?php endif; ?>
							
							<?php if ( get_field( 'x5_service_service_arrow_show' ) ): ?>
								<span class="c-pointer">or</span>
							<?php endif; ?>

						</div>
						<!-- cta-holder -->

					<?php endif; ?>

				</div>
				<!-- info -->
			
			</div>
			<!-- o-holder -->

		</div>
		<!-- / o-container -->

	</section>
	<!-- c-service -->

<?php endif; ?>

<?php get_template_part( 'partials/contact', 'section' ); ?>

<?php get_footer();
