<?php
/**
 * X5: Header
 *
 * Contains dummy HTML to show the default structure of WordPress header file
 *
 * Remember to always include the wp_head() call before the ending </head> tag
 *
 * Make sure that you include the <!DOCTYPE html> in the same line as ?> closing tag
 * otherwise ajax might not work properly
 *
 * @package WordPress
 * @subpackage X5
 */
?><!DOCTYPE html>
<!--[if IE 8]> <html class="no-js ie8 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <meta name="viewport" content="initial-scale=1, minimum-scale=1, width=device-width">
  <meta name="format-detection" content="telephone=no">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  
  <!-- optional -->
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <!-- end of optional -->
  
  <script>
    document.createElement('picture');
  </script>

  <script>
    document.querySelector('html').classList.remove('no-js');
  </script>
  
  <?php
  	
  	// load gravity forms scripts
    gravity_form_enqueue_scripts( 1, true );
  	
    // do not remove
    wp_head(); 
  ?>
    
</head>

<body
  <?php
    if ( is_page_template( 'page-home.php' ) ){
      body_class( 'page-home' );
    } elseif ( is_page_template( 'page-about.php' ) ){
      body_class( 'page-about' );
    } elseif ( is_page_template( 'page-service.php' )) {
      body_class( 'page-service' );
    } elseif ( is_page_template( 'page-contact.php' ) ){
      body_class( 'page-contact' );
    } elseif ( is_page_template( 'page-blog.php' )) {
      body_class( 'page-blog' );
    } elseif ( is_page()) {
      body_class( 'page-default' );
    }
  ?>>
  
  <header class="c-header">
  
    <div class="o-container">
      
      <h1 class="c-logo"><a href="<?php echo esc_url( home_url() ); ?>" rel="index" title="<?php esc_html_e( 'Go to homepage', 'x5' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
      
      <div class="c-nav js-nav">

        <div class="c-nav__btn js-nav__btn"></div>
        <nav class="c-nav__menu js-nav__menu">
          
          <!-- / Display menu -->
          <?php
            wp_nav_menu(
              array(
                'theme_location' => 'header-menu',
                'container' => '',
              )
            );
          ?>
          
          <div class="c-utility-nav">
            
            <?php if ( get_field( 'x5_header_cbtn_text', 'option' ) &&
                       get_field( 'x5_header_cbtn_link', 'option' ) ): ?>
              <a href="<?php echo esc_url( get_field ( 'x5_header_cbtn_link', 'option' ) ); ?>" class="c-btn c-btn--small"><?php echo esc_html( get_field( 'x5_header_cbtn_text', 'option' ) ); ?></a>
            <?php endif; ?>
            
            
            <?php get_template_part( 'partials/social', 'buttons' ); ?> 

          </div>
          <!-- / c-utility-nav -->

        </nav>

      </div>
      <!-- / c-nav -->
        
    </div>
    <!-- / o-container -->
  
  </header>
  <!-- / c-header -->