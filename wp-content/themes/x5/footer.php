<?php
/**
 * X5: Footer
 *
 * Remember to always include the wp_footer() call before the </body> tag
 *
 * @package WordPress
 * @subpackage X5
 */
?>

<footer class="c-footer">
  
  <div class="o-container">
  	
  	<?php if ( have_rows( 'x5_footer_menus', 'option' ) ): ?>
  		
			<?php while( have_rows( 'x5_footer_menus', 'option' ) ) : the_row(); ?>
				
				<div class="secondary-menu">
					
					<?php if ( get_sub_field( 'x5_footer_menus_title' ) ): ?>
						<h6><?php echo esc_html( get_sub_field( 'x5_footer_menus_title' ) ); ?></h6>
					<?php endif; ?>

  				<?php if ( have_rows( 'x5_footer_menus_items', 'option' ) ): ?>
  				
						<ul>

							<?php while( have_rows( 'x5_footer_menus_items', 'option' ) ) : the_row(); 
								$x5_footer_menus_link = get_sub_field( 'x5_footer_menus_link', 'option' );

								if ( get_sub_field( 'x5_footer_menus_link_ext', 'option' ) ) {
									$x5_footer_menus_link .= esc_html( get_sub_field( 'x5_footer_menus_link_ext', 'option' ) );
								}
								
								$x5_footer_menus_text = get_sub_field( 'x5_footer_menus_text', 'option' );
								
							?>
											
								<li><a href="<?php echo esc_url( $x5_footer_menus_link ); ?>"><?php echo esc_html( $x5_footer_menus_text ); ?></a></li>
											
							<?php endwhile; ?>
								
						</ul>		
  				
  				<?php endif; ?>

  			</div>
				<!-- secondary-menu -->

			<?php endwhile; ?>
  	 		
  	<?php endif; ?>

  	<span class="c-logo"><a href="<?php echo esc_url( home_url() ); ?>"><span><?php bloginfo( 'name' ); ?></span></a></span>
  	
    <?php get_template_part( 'partials/social', 'buttons' ); ?>
		
		<?php if ( get_field( 'x5_footer_copyright', 'option' ) ): ?>
			<div class="copyright"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_footer_copyright', 'option' ) ) ); ?></div>
		<?php endif; ?>
  	
		<?php if ( get_field( 'x5_footer_attribution', 'option' ) ): ?>
			<div class="attribution"><?php echo wp_kses_post( force_balance_tags( get_field( 'x5_footer_attribution', 'option' ) ) ); ?></div>
		<?php endif; ?>
  	
  </div>
  <!-- / o-container --> 
  
</footer>

<?php
	// do not remove
	wp_footer();
?>

</body>
</html>
